<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 13.01.2018
 * Time: 13:53
 */

namespace app\socketEvent;

use mkiselev\broadcasting\channels\PrivateChannel;
use mkiselev\broadcasting\events\BroadcastEvent;

class NewMessageAdded extends BroadcastEvent
{
    public $author;
    public $message;

    public function __constructor($author, $message)
    {
        $this->author = $author;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on
     *
     * @return string|array
     */
    public function broadcastOn()
    {
        // TODO: Implement broadcastOn() method.
        return new PrivateChannel('chat');
    }

    public function broadcastAs()
    {
        return 'message';
    }
}