<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 14.01.2018
 * Time: 15:51
 */

namespace app\socketEvent;


use mkiselev\broadcasting\channels\PrivateChannel;
use mkiselev\broadcasting\events\BroadcastEvent;

class NewPositionEvent extends BroadcastEvent
{
    public $position;

    public function __constructor($position)
    {
        $this->position = $position;
    }

    /**
     * Get the channels the event should broadcast on
     *
     * @return string|array
     */
    public function broadcastOn()
    {
        // TODO: Implement broadcastOn() method.
        return new PrivateChannel('position');
    }

    public function broadcastAs()
    {
        return 'go';
    }
}