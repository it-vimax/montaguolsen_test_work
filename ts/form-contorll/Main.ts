import _StepOne = require('./libs/StepOne'); import StepOne = _StepOne.StepOne;
import _StepTwo = require('./libs/StepTwo'); import StepTwo = _StepTwo.StepTwo;
import _StepThree = require('./libs/StepThree'); import StepThree = _StepThree.StepThree;

class Main
{
    static main():void
    {
        $('#telephone_number').mask('+44 (000) 000-00-00');
        let stepOne = new StepOne();
        stepOne.onEvent();
        let setpTwo = new StepTwo();
        setpTwo.onEvent();
        let stepThree = new StepThree();
        stepThree.onEvent();
    }
}

$(document).ready(function(event:any)
{
    Main.main();
});