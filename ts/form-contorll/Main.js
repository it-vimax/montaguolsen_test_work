"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _StepOne = require("./libs/StepOne");
var StepOne = _StepOne.StepOne;
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        $('#telephone_number').mask('+44 (000) 000-00-00');
        var stepOne = new StepOne();
        stepOne.onEvent();
    };
    return Main;
}());
$(document).ready(function (event) {
    Main.main();
});
