/**
 * Created by Maks on 06.02.2018.
 */
export class Message
{
    static show(idElement:string, data:string, status:number = 0):void
    {
        switch (status)
        {
            case 0: // success
                $("#"+idElement).css('color', 'green');
                $("#"+idElement).text(data);
                setTimeout(function()
                {
                    $("#"+idElement).text('');
                }, 5000);
                console.log('ok');
                break;
            case 1: // error
                $("#"+idElement).css('color', 'red');
                $("#"+idElement).text(data);
                setTimeout(function()
                {
                    $("#"+idElement).text('');
                }, 5000);
                break;
        }
    }
}