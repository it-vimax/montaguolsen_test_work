/**
 * Created by Maks on 06.02.2018.
 */
export class Data
{
    static firstName:string;
    static lastName:string;
    static email:string;
    static telephoneNumber:string;
    static db_dd:number;
    static db_mm:number;
    static db_yy:number;
    static gender:number;
    static comments:number;

    static get():any
    {
        let res:any = {
            firstName: Data.firstName,
            lastName: Data.lastName,
            email: Data.email,
            telephoneNumber: Data.telephoneNumber,
            db_dd: Data.db_dd,
            db_mm: Data.db_mm,
            db_yy: Data.db_yy,
            gender: Data.gender,
            comments:Data.comments
        };
        return res;
    }
}