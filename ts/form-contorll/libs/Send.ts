import _Data = require('./Data'); import Data = _Data.Data;
export class Send
{
    static ajax():void
    {
        let csrfParam = $('meta[name="csrf-param"]').attr("content");
        let csrfToken = $('meta[name="csrf-token"]').attr("content");

        $.ajax({
            url: 'site/save',                 // указываем URL
        	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
        	data : {
                data: Data.get(),
                csrfParam : csrfToken
            },
        	async : true,	            //	асинхронность запроса, по умолчанию true
        	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
        	contentType : "application/x-www-form-urlencoded",
        	type : "post",            // GET либо POST

            success: function (data:any)
        	{	// вешаем свой обработчик на функцию success
        	    if(!data)
        	    {
        	        console.warn('Validation server error!');
        	        return;
        	    }
        	    console.log(data);
                setTimeout(function()
                {
                    document.location.href='user/index';
                }, 3000);
            },

        	error: function (error:any)
        	{	// вешаем свой обработчик на функцию error
        		console.log(error);
        	},

        	beforeSend: function(){},	//	срабатывает перед отправкой запроса
        	complete: function(){}		//	срабатывает по окончанию запроса
        });
    }
}