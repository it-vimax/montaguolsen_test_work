import _Validator = require('./Validator'); import Validator = _Validator.Validator;
import _Message = require('./Message'); import Message = _Message.Message;
import _Data = require('./Data'); import Data = _Data.Data;
import _Send = require('./Send'); import Send = _Send.Send;

export class StepThree
{
    public onEvent():void
    {
        let loc = this;
        $('body').on('click', '#btn-step3', ()=>
        {
            let comments = $('#coments').val();
            if(Validator.emptyStr(comments))
            {
                return Message.show('message-step3', 'Empty field comments!', 1);
            }
            Data.comments = comments;
            Send.ajax();
            Message.show('message-step3', 'Thank you!!!');
        });
    }
}