export class Validator
{
    static emptyStr(data:string):boolean
    {
        if(data.length <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static gender(data:number):boolean
    {
        if(data > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}