import _Validator = require('./Validator'); import Validator = _Validator.Validator;
import _Message = require('./Message'); import Message = _Message.Message;
import _Data = require('./Data'); import Data = _Data.Data;

export class StepOne
{
    public onEvent():void
    {
        let loc = this;
        $('body').on('click', '#btn-step1', ()=>
        {
            let firstName = $('#first_name').val();
            let lastName = $('#last_name').val();
            let emailAddress = $('#field-email').val();
            if(Validator.emptyStr(firstName))
            {
                return Message.show('message-step1', 'Empty field First Name!', 1);
            }
            if(Validator.emptyStr(lastName))
            {
                return Message.show('message-step1', 'Empty field Last Name!', 1);
            }
            if(Validator.emptyStr(emailAddress))
            {
                return Message.show('message-step1', 'Empty field Email Name!', 1);
            }
            Data.firstName = firstName;
            Data.lastName = lastName;
            Data.email = emailAddress;
            $("#step-1").slideUp();
            $("#step-2").slideDown();
        });
    }
}