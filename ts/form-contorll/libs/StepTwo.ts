import _Validator = require('./Validator'); import Validator = _Validator.Validator;
import _Message = require('./Message'); import Message = _Message.Message;
import _Data = require('./Data'); import Data = _Data.Data;

export class StepTwo
{
    public onEvent():void
    {
        let loc = this;
        $('body').on('click', '#btn-step2', ()=>
        {
            let telephoneNumber = $('#telephone_number').val();
            let db_dd = $('#db-dd').val();
            let db_mm = $('#db-mm').val();
            let db_yy = $('#db-YY').val();
            let gender = $('#gender option:selected').val();
            if(Validator.emptyStr(telephoneNumber))
            {
                return Message.show('message-step2', 'Empty field Telephone number!', 1);
            }
            if(Validator.emptyStr(db_dd))
            {
                return Message.show('message-step2', 'Empty field day!', 1);
            }
            if(Validator.emptyStr(db_mm))
            {
                return Message.show('message-step2', 'Empty field month!', 1);
            }
            if(Validator.emptyStr(db_yy))
            {
                return Message.show('message-step2', 'Empty field year!', 1);
            }
            if(Validator.gender(gender))
            {
                return Message.show('message-step2', 'Empty field gender!', 1);
            }
            Data.telephoneNumber = telephoneNumber;
            Data.db_dd = db_dd;
            Data.db_mm = db_mm;
            Data.db_yy = db_yy;
            Data.gender = gender;
            $("#step-2").slideUp();
            $("#step-3").slideDown();
        });
    }
}