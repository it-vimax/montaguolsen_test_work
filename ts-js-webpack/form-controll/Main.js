"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _StepOne = require("./libs/StepOne");
var StepOne = _StepOne.StepOne;
var _StepTwo = require("./libs/StepTwo");
var StepTwo = _StepTwo.StepTwo;
var _StepThree = require("./libs/StepThree");
var StepThree = _StepThree.StepThree;
var Main = (function () {
    function Main() {
    }
    Main.main = function () {
        $('#telephone_number').mask('+44 (000) 000-00-00');
        var stepOne = new StepOne();
        stepOne.onEvent();
        var setpTwo = new StepTwo();
        setpTwo.onEvent();
        var stepThree = new StepThree();
        stepThree.onEvent();
    };
    return Main;
}());
$(document).ready(function (event) {
    Main.main();
});
//# sourceMappingURL=Main.js.map