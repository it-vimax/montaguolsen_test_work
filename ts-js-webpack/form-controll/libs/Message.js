"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maks on 06.02.2018.
 */
var Message = (function () {
    function Message() {
    }
    Message.show = function (idElement, data, status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0:
                $("#" + idElement).css('color', 'green');
                $("#" + idElement).text(data);
                setTimeout(function () {
                    $("#" + idElement).text('');
                }, 5000);
                console.log('ok');
                break;
            case 1:
                $("#" + idElement).css('color', 'red');
                $("#" + idElement).text(data);
                setTimeout(function () {
                    $("#" + idElement).text('');
                }, 5000);
                break;
        }
    };
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=Message.js.map