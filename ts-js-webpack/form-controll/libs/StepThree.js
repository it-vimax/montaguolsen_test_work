"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Validator = require("./Validator");
var Validator = _Validator.Validator;
var _Message = require("./Message");
var Message = _Message.Message;
var _Data = require("./Data");
var Data = _Data.Data;
var _Send = require("./Send");
var Send = _Send.Send;
var StepThree = (function () {
    function StepThree() {
    }
    StepThree.prototype.onEvent = function () {
        var loc = this;
        $('body').on('click', '#btn-step3', function () {
            var comments = $('#coments').val();
            if (Validator.emptyStr(comments)) {
                return Message.show('message-step3', 'Empty field comments!', 1);
            }
            Data.comments = comments;
            Send.ajax();
            Message.show('message-step3', 'Thank you!!!');
        });
    };
    return StepThree;
}());
exports.StepThree = StepThree;
//# sourceMappingURL=StepThree.js.map