"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maks on 06.02.2018.
 */
var Data = (function () {
    function Data() {
    }
    Data.get = function () {
        var res = {
            firstName: Data.firstName,
            lastName: Data.lastName,
            email: Data.email,
            telephoneNumber: Data.telephoneNumber,
            db_dd: Data.db_dd,
            db_mm: Data.db_mm,
            db_yy: Data.db_yy,
            gender: Data.gender,
            comments: Data.comments
        };
        return res;
    };
    return Data;
}());
exports.Data = Data;
//# sourceMappingURL=Data.js.map