"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Validator = (function () {
    function Validator() {
    }
    Validator.emptyStr = function (data) {
        if (data.length <= 0) {
            return true;
        }
        else {
            return false;
        }
    };
    Validator.gender = function (data) {
        if (data > 0) {
            return false;
        }
        else {
            return true;
        }
    };
    return Validator;
}());
exports.Validator = Validator;
//# sourceMappingURL=Validator.js.map