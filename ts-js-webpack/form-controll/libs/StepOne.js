"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Validator = require("./Validator");
var Validator = _Validator.Validator;
var _Message = require("./Message");
var Message = _Message.Message;
var _Data = require("./Data");
var Data = _Data.Data;
var StepOne = (function () {
    function StepOne() {
    }
    StepOne.prototype.onEvent = function () {
        var loc = this;
        $('body').on('click', '#btn-step1', function () {
            var firstName = $('#first_name').val();
            var lastName = $('#last_name').val();
            var emailAddress = $('#field-email').val();
            if (Validator.emptyStr(firstName)) {
                return Message.show('message-step1', 'Empty field First Name!', 1);
            }
            if (Validator.emptyStr(lastName)) {
                return Message.show('message-step1', 'Empty field Last Name!', 1);
            }
            if (Validator.emptyStr(emailAddress)) {
                return Message.show('message-step1', 'Empty field Email Name!', 1);
            }
            Data.firstName = firstName;
            Data.lastName = lastName;
            Data.email = emailAddress;
            $("#step-1").slideUp();
            $("#step-2").slideDown();
        });
    };
    return StepOne;
}());
exports.StepOne = StepOne;
//# sourceMappingURL=StepOne.js.map