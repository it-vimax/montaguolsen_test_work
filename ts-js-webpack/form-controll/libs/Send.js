"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Data = require("./Data");
var Data = _Data.Data;
var Send = (function () {
    function Send() {
    }
    Send.ajax = function () {
        var csrfParam = $('meta[name="csrf-param"]').attr("content");
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            url: 'site/save',
            //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            dataType: "json",
            data: {
                data: Data.get(),
                csrfParam: csrfToken
            },
            async: true,
            cache: true,
            contentType: "application/x-www-form-urlencoded",
            type: "post",
            success: function (data) {
                if (!data) {
                    console.warn('Validation server error!');
                    return;
                }
                console.log(data);
                setTimeout(function () {
                    document.location.href = 'user/index';
                }, 3000);
            },
            error: function (error) {
                console.log(error);
            },
            beforeSend: function () { },
            complete: function () { } //	срабатывает по окончанию запроса
        });
    };
    return Send;
}());
exports.Send = Send;
//# sourceMappingURL=Send.js.map