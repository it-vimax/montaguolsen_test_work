"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _Validator = require("./Validator");
var Validator = _Validator.Validator;
var _Message = require("./Message");
var Message = _Message.Message;
var _Data = require("./Data");
var Data = _Data.Data;
var StepTwo = (function () {
    function StepTwo() {
    }
    StepTwo.prototype.onEvent = function () {
        var loc = this;
        $('body').on('click', '#btn-step2', function () {
            var telephoneNumber = $('#telephone_number').val();
            var db_dd = $('#db-dd').val();
            var db_mm = $('#db-mm').val();
            var db_yy = $('#db-YY').val();
            var gender = $('#gender option:selected').val();
            if (Validator.emptyStr(telephoneNumber)) {
                return Message.show('message-step2', 'Empty field Telephone number!', 1);
            }
            if (Validator.emptyStr(db_dd)) {
                return Message.show('message-step2', 'Empty field day!', 1);
            }
            if (Validator.emptyStr(db_mm)) {
                return Message.show('message-step2', 'Empty field month!', 1);
            }
            if (Validator.emptyStr(db_yy)) {
                return Message.show('message-step2', 'Empty field year!', 1);
            }
            if (Validator.gender(gender)) {
                return Message.show('message-step2', 'Empty field gender!', 1);
            }
            Data.telephoneNumber = telephoneNumber;
            Data.db_dd = db_dd;
            Data.db_mm = db_mm;
            Data.db_yy = db_yy;
            Data.gender = gender;
            $("#step-2").slideUp();
            $("#step-3").slideDown();
        });
    };
    return StepTwo;
}());
exports.StepTwo = StepTwo;
//# sourceMappingURL=StepTwo.js.map