<?php

namespace app\controllers;

use app\models\Customer;
use app\models\SignalEvent;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSave()
    {
        if(Yii::$app->request->isPost)
        {
            $data = Yii::$app->request->post('data');
            $dayBirth = $data['db_yy'].'-'.$data['db_mm'].'-'.$data['db_dd'];
            $user = new User();
            $user->first_name = $data['firstName'];
            $user->last_name = $data['lastName'];
            $user->email = $data['email'];
            $user->telephone = $data['telephoneNumber'];
            $user->date_birth = $dayBirth;
            $user->gender = $data['gender'];
            $user->comments = $data['comments'];
            if($user->save())
            {
                return $this->asJson($data);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return $this->redirect('/');
        }
    }

    public function actionResult()
    {
        return $this->asJson('result');
    }
}
