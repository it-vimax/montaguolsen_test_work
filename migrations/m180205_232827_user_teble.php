<?php

use yii\db\Migration;

/**
 * Class m180205_232827_user_teble
 */
class m180205_232827_user_teble extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'email' => $this->string(),
            'telephone' => $this->string(),
            'date_birth' => $this->date(),
            'gender' => $this->integer(),
            'comments' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180205_232827_user_teble cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180205_232827_user_teble cannot be reverted.\n";

        return false;
    }
    */
}
