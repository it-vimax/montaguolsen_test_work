<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $telephone
 * @property string $date_birth
 * @property int $gender
 * @property string $comments
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_birth'], 'safe'],
            [['gender'], 'integer'],
            [['comments'], 'string'],
            [['first_name', 'last_name', 'email', 'telephone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'telephone' => 'Telephone',
            'date_birth' => 'Date Birth',
            'gender' => 'Gender',
            'comments' => 'Comments',
        ];
    }
}
