<?php

/* @var $this yii\web\View */
use app\assets\AppAsset;

AppAsset::register($this);
$this->title = 'Test work Bukach Maksim';
?>

<div class="container">
    <div class="window">
        <div class="title">
            <span class="step-title">Step 1: Your details</span>
        </div>
        <div class="step" id="step-1">
            <div class="form">
                <div class="column">
                    <label for="first_name">First Name</label>
                    <input type="text" class="field-text" id="first_name">
                    <label for="first_name">Last Name</label>
                    <input type="text" class="field-text" id="last_name">
                </div>
                <div class="column">
                    <label for="first_name">Email Address</label>
                    <input type="email" class="field-text" id="field-email">
                </div>
                <div class="info"  id="info-step1">
                    <span id="message-step1"></span>
                </div>
                <input type="button" class="btn" id="btn-step1" value="Next >">
            </div>
        </div>
        <div class="title">
            <span class="step-title">Step 2: More details</span>
        </div>
        <div class="step hide" id="step-2">
            <div class="form">
                <div class="column">
                    <label for="telephone_number">Telephone number</label>
                    <input type="text" class="field-text" id="telephone_number">

                    <label  for="gender">Gender:</label>
                    <select id="gender" class="form-control input-small input-inline btn-select ">
                        <option value="0" selected>Select Gender</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>

                </div>
                <div class="column">
                    <label for="first_name">Date of birth</label>
                    <div class="db-container">
                        <input type="text" class="field-text field-db" maxlength="2" id="db-dd">
                        <input type="text" class="field-text field-db" maxlength="2" id="db-mm">
                        <input type="text" class="field-text field-db" maxlength="4" id="db-YY">
                    </div>
                </div>
                <div class="info"  id="info-step2">
                    <span id="message-step2"></span>
                </div>
                <input type="button" class="btn" id="btn-step2" value="Next >">
            </div>
        </div>
        <div class="title">
            <span class="step-title">Step 3: Final details</span>
        </div>
        <div class="step hide" id="step-3">
            <div class="form">
                <div class="column">
                    <label for="coments">Cumments</label>
                    <textarea id="coments" class="field-text field-comments"></textarea>
                </div>

                <div class="info" id="info-step3">
                    <span id="message-step3"></span>
                </div>
                <input type="button" class="btn" id="btn-step3" value="Next >">
            </div>
        </div>
    </div>
</div>

