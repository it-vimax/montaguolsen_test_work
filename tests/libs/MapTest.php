<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 30.01.2018
 * Time: 17:24
 */

namespace app\tests\libs;

use libs\Map;
use PHPUnit\Framework\TestCase;

class MapTest extends TestCase
{
    public function testHello()
    {
        $map = new Map();
        $this->assertEquals($map->hello(), 'hello');
    }
}
